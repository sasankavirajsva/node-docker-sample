var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

// save roster 
router.post('/saveRoster', async function (req, res, next) {
    const API_NAME = 'saveRoster POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log(receivedObj);
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');
        var rosterCode = '';

        if (null == receivedObj.values.rosterId || undefined == receivedObj.values.rosterId) {

        } else {
            // update roster old record is_active=0
            const [updateRoster, fields1] = await conn.query("update hrm_roster set IS_ACTIVE=0 where ID_ROSTER=" + receivedObj.values.rosterId + "");
            rosterCode = receivedObj.values.rosterCode;

        }

        const [resultData, fields] = await conn.query("select * from hrm_roster where IS_ACTIVE=1 and ROSTER_NAME='" + receivedObj.values.rosterName + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('ROSTER ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            var empAr = [];

            // check these employees in old active roster in this date range
            if (receivedObj.employeeAr.length > 0) {
                for (var k = 0; k < receivedObj.employeeAr.length; k++) {
                    const [resultEmps, fields1] = await conn.query("select * from view_ros_detail_shift_emp_loc where IS_ACTIVE=1 and ID_EMPLOYEE_PROFILE=" + receivedObj.employeeAr[k].employeeProId + " group by ID_ROSTER");

                    // console.log(resultEmps);
                    var rosterFromDate = new Date(receivedObj.values.startDate).getMonth() + "/" + new Date(receivedObj.values.startDate).getDate() + "/" + new Date(receivedObj.values.startDate).getFullYear();
                    var rosterToDate = new Date(receivedObj.values.endDate).getMonth() + "/" + new Date(receivedObj.values.endDate).getDate() + "/" + new Date(receivedObj.values.endDate).getFullYear();

                    if (resultEmps.length > 0) {
                        for (var l = 0; l < resultEmps.length; l++) {
                            var empFromDate = new Date(resultEmps[l].FROM_DATE).getMonth() + "/" + new Date(resultEmps[l].FROM_DATE).getDate() + "/" + new Date(resultEmps[l].FROM_DATE).getFullYear();
                            var empToDate = new Date(resultEmps[l].TO_DATE).getMonth() + "/" + new Date(resultEmps[l].TO_DATE).getDate() + "/" + new Date(resultEmps[l].TO_DATE).getFullYear();

                            // console.log(rosterFromDate, " ", rosterToDate);
                            // console.log(empFromDate, " ", empToDate);

                            if ((empFromDate >= rosterFromDate && empFromDate <= rosterToDate) || (rosterFromDate >= empFromDate && rosterFromDate <= empToDate)) {

                                console.log("employee in another roster in this date range");
                                if (resultEmps[l].ID_SHIFT == receivedObj.values.shiftCode) {
                                    empAr.push({
                                        ID_EMPLOYEE_PROFILE: resultEmps[l].ID_EMPLOYEE_PROFILE,
                                        ROSTER_NAME: resultEmps[l].ROSTER_NAME,
                                        FROM_DATE: resultEmps[l].FROM_DATE,
                                        TO_DATE: resultEmps[l].TO_DATE,
                                        SHIFT_NAME: resultEmps[l].SHIFT_NAME,
                                        SHIFT_CODE: resultEmps[l].SHIFT_CODE,
                                        ID_EMPLOYEE_REGISTRY: resultEmps[l].ID_EMPLOYEE_REGISTRY,
                                        REGISTRY_CODE_PREFIX: resultEmps[l].REGISTRY_CODE_PREFIX,
                                        LOCATION_NAME: resultEmps[l].LOCATION_NAME,
                                        FIRSTNAME: resultEmps[l].FIRSTNAME,
                                        MIDDLENAME: resultEmps[l].MIDDLENAME,
                                        LASTNAME: resultEmps[l].LASTNAME,
                                        ETF_NO: resultEmps[l].ETF_NO,
                                        NIC_NO: resultEmps[l].NIC_NO,
                                        GENDER: resultEmps[l].GENDER,
                                        MOBILE: resultEmps[l].MOBILE,
                                        DESIGNATION: resultEmps[l].DESIGNATION,
                                        DEPARTMENT: resultEmps[l].DEPARTMENT,
                                        GRADE_NAME: resultEmps[l].GRADE_NAME,
                                        EMPLOYMENT_NAME: resultEmps[l].EMPLOYMENT_NAME,
                                    });
                                }
                            } else {
                                console.log("employee not in another roster in this date range");
                            }
                        }
                    }
                }
            }

            var uniqueEmpAr = [];

            for (let m = 0; m < empAr.length; m++) {
                if (uniqueEmpAr.indexOf(empAr[m].ID_EMPLOYEE_PROFILE) < 0) {
                    uniqueEmpAr.push(empAr[m])
                }
            }

            if (empAr.length == 0) {

                // set data to insert
                var roster = {
                    ROSTER_CODE: rosterCode,
                    ROSTER_NAME: receivedObj.values.rosterName,
                    FROM_DATE: receivedObj.values.startDate,
                    TO_DATE: receivedObj.values.endDate,
                    NO_OF_EMPLOYEES: receivedObj.employeeAr.length,
                    IS_ACTIVE: 1
                };

                // insert data into roster table
                const [resultRosterSave, fields1] = await conn.query('INSERT INTO hrm_roster SET ?', roster);
                logger.info('Successfully saved Roster record id = ' + resultRosterSave.insertId);

                // genarate roster code
                if (null == receivedObj.values.rosterId || undefined == receivedObj.values.rosterId) {
                    const [resultGenarateRosterCode, fields2] = await conn.query("update hrm_roster set ROSTER_CODE='R" + resultRosterSave.insertId + "' where ID_ROSTER=" + resultRosterSave.insertId + "");
                    logger.info('Successfully Genarated Roster Code = R' + resultRosterSave.insertId);
                }

                // insert data into roster details table
                if (receivedObj.employeeAr.length > 0) {
                    for (var i = 0; i < receivedObj.employeeAr.length; i++) {

                        //2. get shift details where shift id 
                        const [resultShiftTimes, fields3] = await conn.query("select * from hrm_shift_time_allocation where IS_ACTIVE=1 and ID_SHIFT=" + receivedObj.values.shiftCode + "");

                        //3. get date count of roster start and end date range
                        var dateRangeAr = getDates(new Date(receivedObj.values.startDate), new Date(receivedObj.values.endDate));
                        dateRangeAr.forEach(async function (date) {

                            var dayName = '';
                            var day = '';
                            var startTime = '';
                            var endTime = '';
                            var otStart = '';
                            var otEnd = '';
                            var otRate = '';
                            var lateStart = '';
                            var isOverNight = '';

                            for (var j = 0; j < resultShiftTimes.length; j++) {

                                if ('MONDAY' == resultShiftTimes[j].DAY && 'Mon' == date.toString().split(' ')[0]) {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                } if ('TUESDAY' == resultShiftTimes[j].DAY && 'Tue' == date.toString().split(' ')[0]) {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                }
                                if ('WEDNESDAY' == resultShiftTimes[j].DAY && 'Wed' == date.toString().split(' ')[0]) {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                }
                                if ('THURSDAY' == resultShiftTimes[j].DAY && 'Thu' == date.toString().split(' ')[0]) {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                }
                                if ('FRIDAY' == resultShiftTimes[j].DAY && 'Fri' == date.toString().split(' ')[0]) {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                }
                                if ('SATURDAY' == resultShiftTimes[j].DAY && 'Sat' == date.toString().split(' ')[0]) {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                }
                                if ('SUNDAY' == resultShiftTimes[j].DAY && 'Sun' == date.toString().split(' ')[0]) {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                }
                                if ('MONDAY' == resultShiftTimes[j].DAY || 'TUESDAY' == resultShiftTimes[j].DAY
                                    || 'WEDNESDAY' == resultShiftTimes[j].DAY || 'THURSDAY' == resultShiftTimes[j].DAY
                                    || 'FRIDAY' == resultShiftTimes[j].DAY || 'SATURDAY' == resultShiftTimes[j].DAY
                                    || 'SUNDAY' == resultShiftTimes[j].DAY) {

                                } else {
                                    dayName = resultShiftTimes[j].DAY;
                                    day = date;
                                    startTime = resultShiftTimes[j].START_TIME;
                                    endTime = resultShiftTimes[j].END_TIME;
                                    otStart = resultShiftTimes[j].OT_START;
                                    otEnd = resultShiftTimes[j].OT_END;
                                    otRate = resultShiftTimes[j].OT_RATE;
                                    lateStart = resultShiftTimes[j].LATE_START;
                                    isOverNight = resultShiftTimes[j].IS_OVER_NIGHT;
                                }
                            }

                            //4. insert data into roster details table
                            var rosterDetails = {
                                ID_SHIFT: receivedObj.values.shiftCode,
                                ID_ROSTER: resultRosterSave.insertId,
                                ID_EMPLOYEE_GROUP: 0,
                                ID_EMPLOYEE_PROFILE: receivedObj.employeeAr[i].employeeProId,
                                DAY_NAME: dayName,
                                DAY: day,
                                START_TIME: startTime,
                                END_TIME: endTime,
                                OT_START: otStart,
                                OT_END: otEnd,
                                LATE_START: lateStart,
                                OT_RATE: otRate,
                                IS_OVER_NIGHT: isOverNight,
                                IS_ACTIVE: 1
                            };

                            // insert data into roster detail table
                            const [resultRosterDetailSave, fields4] = await conn.query('INSERT INTO hrm_roster_details SET ?', rosterDetails);
                            logger.info('Successfully saved Roster Detail record id = ' + resultRosterDetailSave.insertId);
                        });
                    }
                } else {
                    await conn.query('COMMIT');
                    conn.release();
                    logger.info('NO EMPLOYEES IN THIS GROUP');
                    return res.status(200).send({ success: false, message: 'NO_EMPS' });
                }
            } else {
                await conn.query('COMMIT');
                conn.release();
                logger.info('EMPLOYEE ALREADY ASSIGNED THIS DATE RANGE ROSTER');
                return res.status(200).send({ success: false, message: 'ALREADY_ASSIGN', empAr: uniqueEmpAr });
            }
        }

        await conn.query('COMMIT');
        conn.release();
        logger.info('ROSTER CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('ROSTER CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get date range 
var getDates = function (startDate, endDate) {
    var dates = [],
        currentDate = startDate,
        addDays = function (days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        };
    while (currentDate <= endDate) {
        dates.push(currentDate);
        currentDate = addDays.call(currentDate, 1);
    }
    return dates;
};

router.post('/getRosters', async function (req, res, next) {
    const API_NAME = 'getRosters POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        var sql = "";
        var sqlCount = "";
        if ('' == receivedObj.searchVal || undefined == receivedObj.searchVal || null == receivedObj.searchVal) {
            sql = "select * from hrm_roster where IS_ACTIVE=1 order by ID_ROSTER desc limit 10 offset " + receivedObj.pageSize + "";
            sqlCount = "select count(*) as count_of from hrm_roster where IS_ACTIVE=1";
        } else {
            sql = "select * from hrm_roster where IS_ACTIVE=1 and (ID_SHIFT =" + receivedObj.searchVal + " or ID_ROSTER =" + receivedObj.searchVal + ") order by ID_ROSTER desc limit 10 offset " + receivedObj.pageSize;
            sqlCount = "select count(*) as count_of from hrm_roster where IS_ACTIVE=1 and (ID_SHIFT =" + receivedObj.searchVal + " or ID_ROSTER =" + receivedObj.searchVal + ")";

        }

        const [resultData, fields] = await conn.query(sql);
        const [resultDataCount, fields1] = await conn.query(sqlCount);

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ROSTERs SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET ROSTERs DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete rosters 
router.post('/deleteRoster', async function (req, res, next) {
    const API_NAME = 'deleteRoster POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        var sql = "UPDATE hrm_roster SET IS_ACTIVE=0 WHERE ID_ROSTER=" + receivedObj.rowObj.rosterId + "";

        const [resultData, fields] = await conn.query(sql);

        await conn.query('COMMIT');
        conn.release();
        logger.info('ROSTER DELETED SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultData });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT ROSTER DELETED DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get roster details where roster id
router.post('/getRosterDetails', async function (req, res, next) {
    const API_NAME = 'getRosterDetails POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log(receivedObj)

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        var sql = "";
        var sql2 = "";

        if ('' == receivedObj.searchVal || undefined == receivedObj.searchVal || null == receivedObj.searchVal) {
            sql = "select * from view_ros_detail_shift_emp_loc where IS_ACTIVE=1 and ID_ROSTER=" + receivedObj.idRoster + " order by ID_ROSTER_DETAIL desc";
            sql2 = "select * from view_ros_detail_shift_emp_loc where IS_ACTIVE=1 and ID_ROSTER=" + receivedObj.idRoster + " group by ID_EMPLOYEE_PROFILE";
        } else {
            sql = "select * from view_ros_detail_shift_emp_loc where IS_ACTIVE=1 and (ID_SHIFT =" + receivedObj.searchVal + " ID_ROSTER =" + receivedObj.searchVal + " or ID_EMPLOYEE_PROFILE=" + receivedObj.searchVal + ") order by ID_ROSTER_DETAIL desc";
            sql2 = "select * from view_ros_detail_shift_emp_loc where IS_ACTIVE=1 and (ID_SHIFT =" + receivedObj.searchVal + " ID_ROSTER =" + receivedObj.searchVal + " or ID_EMPLOYEE_PROFILE=" + receivedObj.searchVal + ") group by ID_EMPLOYEE_PROFILE";
        }

        const [resultData, fields] = await conn.query(sql);
        const [resultData2, fields2] = await conn.query(sql2);

        var obj = {
            resultData,
            resultData2
        };

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ROSTER DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, obj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET ROSTER DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// update roster details
router.post('/updateRosterDetail', async function (req, res, next) {
    const API_NAME = 'updateRosterDetail POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // set is active = 0 old roster detail id
        const [resultUpdate, fields1] = await conn.query("UPDATE hrm_roster_details SET IS_ACTIVE=0 where ID_ROSTER_DETAIL=" + receivedObj.rosterDetailId + "");
        logger.info('Successfully Updated set is active=0 Roster detail id = ' + receivedObj.rosterDetailId);

        var rosterDetailObj = {
            ID_SHIFT: receivedObj.shiftId,
            ID_ROSTER: receivedObj.rosterId,
            ID_EMPLOYEE_GROUP: receivedObj.groupId,
            ID_EMPLOYEE_PROFILE: receivedObj.empProfileId,
            DAY_NAME: receivedObj.dayType,
            DAY: receivedObj.date,
            START_TIME: receivedObj.inTime,
            END_TIME: receivedObj.outTime,
            OT_START: receivedObj.otStart,
            OT_END: receivedObj.otEnd,
            LATE_START: receivedObj.lateStart,
            OT_RATE: receivedObj.otRate,
            IS_OVER_NIGHT: receivedObj.isOverNight,
            IS_ACTIVE: 1
        }

        const [resultUpdateRosterDetail, fields2] = await conn.query('INSERT INTO hrm_roster_details SET ?', rosterDetailObj);
        logger.info('Successfully updated(save) Roster Detail record id = ' + resultUpdateRosterDetail.insertId);

        await conn.query('COMMIT');
        conn.release();
        logger.info('ROSTER DETAIL UPDATE(SAVE) TRANSACTION SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESS' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('ROSTER DETAIL UPDATE(SAVE) TRANSACTION FAIL DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete roster detail
router.post('/deleteRosterDetail', async function (req, res, next) {
    const API_NAME = 'deleteRosterDetail POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log(receivedObj)

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        var sql = "UPDATE hrm_roster_details SET IS_ACTIVE=0 WHERE ID_ROSTER_DETAIL=" + receivedObj.idRosterDetails + "";

        const [resultData, fields] = await conn.query(sql);

        await conn.query('COMMIT');
        conn.release();
        logger.info('ROSTER DETAIL DELETED SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultData });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT ROSTER DETAIL DELETED DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

module.exports = router;